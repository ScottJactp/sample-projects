package com.scottjay.mytests.androidtestproject

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.scottjay.mytests.androidtestproject.util.database.DatabaseUtil
import com.scottjay.mytests.androidtestproject.util.database.TextMessage
import com.scottjay.mytests.androidtestproject.util.database.TextMessageDAO
import com.scottjay.mytests.androidtestproject.util.network.NetworkDAO
import com.scottjay.mytests.androidtestproject.util.network.requestentities.UserResponse
import org.json.JSONException
import org.json.JSONObject

object AppRepository {
    const val TAG = "AppRepository"

    private var mApplication: Application = App.getApplication()

    // Data models
    private var mDB: DatabaseUtil.AppDatabase

    // Data access objects
    private var textMessageDAO: TextMessageDAO

    private var networkDAO: NetworkDAO

    init {
        mDB = DatabaseUtil.getDB(mApplication)

        textMessageDAO = mDB.textMessageDAO()
        networkDAO = NetworkDAO()
    }

    fun getTextMessages(userId: String): LiveData<List<TextMessage>> {
        Thread {
            try {
                val jsonObject = JSONObject(networkDAO.tempMessages)
                val messagesArray = jsonObject.getJSONArray("messages")

                Log.e(TAG, "Doing stuff")

                for (i in 0 until messagesArray.length()) {
                    val message = TextMessage.parseMessage(messagesArray.getString(i))
                    message?.let {
                        Log.e(TAG, "message not null ${it.messageBody}")
                        textMessageDAO.insert(it)
                    }
                }
            }
            catch (e: JSONException) { Log.e(TAG, "Exception") }
        }.start()

        return textMessageDAO.selectMessagesFromUser(userId)
    }

    fun insertMessage(message: TextMessage) {
        textMessageDAO?.insert(message)
    }

    fun createUser(userName: String, password: String): LiveData<UserResponse> {
        val liveData = MutableLiveData<UserResponse>()

        Thread {
            liveData.postValue(networkDAO.createUser(userName, password))
        }

        return liveData
    }

    /**
     * Attempts to log in using the given username and password
     * status will be posted to the given live data if there is one
     */
    fun loginUser(userName: String, password: String, liveData: MutableLiveData<Status>? = null) {
        Thread {
            val networkResponse = networkDAO.loginUser(userName, password)
            Log.e(TAG, networkResponse.toString())
            if (networkResponse.code.equals("success")) {
                // Good, we logged in. Do stuff to log in the app
            }
            else {
                // Failed to log in, let the livedata observer know
                liveData?.let {
                    val status = Status()
                    status.status = Status.Companion.STATUS.FAILED
                    status.reason = networkResponse.reason
                    it.postValue(status)
                }
            }
        }.start()
    }
}