package com.scottjay.mytests.androidtestproject

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.scottjay.mytests.androidtestproject.util.database.TextMessage

class TextMessagesRecyclerAdapter(): RecyclerView.Adapter<TextMessagesRecyclerAdapter.ViewHolder>() {

    companion object {
        val TAG = "TextMessagesRecyclerAdapter"

        val VIEW_TYPE_FROM_ME = 0
        val VIEW_TYPE_TO_ME = 1
    }

    var list: List<TextMessage> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): TextMessagesRecyclerAdapter.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        if (viewType == VIEW_TYPE_FROM_ME) {
            val view = LayoutInflater.from(parent!!.context).inflate(R.layout.message_item_from_me, parent, false)
            viewHolder = ViewHolder(view)
            viewHolder.messageBody = viewHolder.itemView.findViewById(R.id.from_message_text)
            viewHolder.messagetime = viewHolder.itemView.findViewById(R.id.from_message_time)
        }
        else {
            val view = LayoutInflater.from(parent!!.context).inflate(R.layout.message_item_to_me, parent, false)
            viewHolder = ViewHolder(view)
            viewHolder.messageBody = viewHolder.itemView.findViewById(R.id.to_message_text)
            viewHolder.messagetime = viewHolder.itemView.findViewById(R.id.to_message_time)
        }

        return viewHolder!!
    }

    override fun onBindViewHolder(holder: TextMessagesRecyclerAdapter.ViewHolder?, position: Int) {
        holder?.let {
            it.dateHeader?.visibility = View.GONE
            it.messageBody?.text = list[position].messageBody
            it.messagetime?.text = "${list[position].messageTime}"
            it.itemView.invalidate()
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (list[position].fromId.equals("A")) 1 else 0
    }

    fun changeList(list: List<TextMessage>) {
        this.list = list
        notifyDataSetChanged()
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        var dateHeader: TextView? = null
        var messageBody: TextView? = null
        var messagetime: TextView? = null
    }
}