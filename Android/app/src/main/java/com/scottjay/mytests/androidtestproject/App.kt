package com.scottjay.mytests.androidtestproject

import android.app.Application
import android.content.Context

class App : Application() {

    companion object {
        private var instance: App? = null

        fun getApplicationContext(): Context {
            return instance!!.applicationContext
        }

        fun getApplication(): Application {
            return instance!!
        }
    }

    init {
        instance = this
    }




}