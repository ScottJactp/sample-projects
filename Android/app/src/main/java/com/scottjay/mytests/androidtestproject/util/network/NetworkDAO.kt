package com.scottjay.mytests.androidtestproject.util.network

import android.util.Log
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.scottjay.mytests.androidtestproject.util.Entities.User
import com.scottjay.mytests.androidtestproject.util.network.requestentities.UserResponse
import java.io.BufferedReader
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.URL

class NetworkDAO {

    companion object {
        private val TAG = "NetworkDAO"

        private val URL_BASE = "http://192.168.1.23:8080" // local machine server
        private val API_BASE = "/"

        private val REQUEST_CREATE_USER = "user/create"
        private val REQUEST_LOGIN_USER = "user/login"

        private val GET_MESSAGES = "messages"
    }

    fun getDefaultHeaders(): HashMap<String, String> {
        val map = HashMap<String, String>()
        map.put("Accept-Language", "en-US")
        return map
    }

    fun addJSONHeaders(map: HashMap<String, String>): HashMap<String, String> {
        map.put("Content-Type", "application/json")
        map.put("Accept-Type", "application/json")
        return map
    }

    fun addAuthToken(headers: HashMap<String, String>): HashMap<String, String> {
        headers.put("AuthToken", "ff")

        return headers
    }

    fun getRequest(address: String, headers: Map<String, String> = getDefaultHeaders()): String {
        val url = URL(address)
        val connection = url.openConnection() as HttpURLConnection

        connection.setRequestMethod("GET")

        // Add headers
        headers.forEach{key, value -> connection.setRequestProperty(key, value)}

        val responseCode = connection.responseCode

        val reader = BufferedReader(
                InputStreamReader(connection.inputStream))

        val response = StringBuffer()
        for (line in reader.lines()) {
            response.append(line)
        }
        reader.close()

        Log.e(TAG, response.toString())

        return response.toString()
    }

    fun postRequest(address: String, requestBody: String, headers: Map<String, String> = getDefaultHeaders()): String {
        val url = URL(address)
        val connection = url.openConnection() as HttpURLConnection

        connection.requestMethod = "POST"

        headers.forEach{key, value -> connection.setRequestProperty(key, value)}


        val response = StringBuffer()

        try {
            connection.doOutput = true
            val dataOutputStream = DataOutputStream(connection.outputStream)
            dataOutputStream.writeBytes(requestBody)
            dataOutputStream.flush()
            dataOutputStream.close()

            val responseCode = connection.responseCode

            val reader = BufferedReader(InputStreamReader(connection.inputStream))

            for (line in reader.lines()) response.append(line)
            reader.close()
        }
        catch (e: ConnectException) {
            // Failed to connect
            response.append("{\"code\":\"failed\",\"reason\":\"Unable to connect to server\"}}")
        }
        catch (e: Exception) {
            // Some generic exception happened
            response.append("{\"code\":\"failed\",\"reason\":\"An unknown error occurred: ${e.message}\"}}")
        }

        Log.e(TAG, response.toString())

        return response.toString()
    }

    val tempMessages = "{\"messages\": [{\"id\":\"a\", \"fromId\":\"A\", \"toId\":\"B\", \"messageBody\":\"helltesttrjf ffvjklnef wvjnfffff frfffff f;flknvsttesttrjf ffvjklnef wvjnfffff frfffff f;flknvsto\", \"messageTime\":60},{\"id\":\"b\", \"fromId\":\"B\", \"toId\":\"A\", \"messageBody\":\"hfregopnfjn;v rovnaoeunrbv ea;robknesfbponk beojnbqerobjna f;objnaefbounaef beafokni\", \"messageTime\":70},{\"id\":\"c\", \"fromId\":\"A\", \"toId\":\"B\", \"messageBody\":\"what's going on\", \"messageTime\":80},{\"id\":\"d\", \"fromId\":\"B\", \"toId\":\"A\", \"messageBody\":\"nothing\", \"messageTime\":90},{\"id\":\"e\", \"fromId\":\"A\", \"toId\":\"A\", \"messageBody\":\"me too\", \"messageTime\":100}]}"

    val tempUserId = "5d3ca42508214c6686339540"
    /**
     * Makes a request to the server for a list of messages for the given user starting from the given time
     */
    fun getMessageString(userId: String, messageTime: Long = 0L): String {

        return getRequest(URL_BASE + API_BASE + GET_MESSAGES)

        return tempMessages
    }

    /**
     * Attempts to create a user on the server
     * @return user or null if user not created
     */
    fun createUser(userName: String, password: String): UserResponse? {

        // Create user with the username and password
        val user = User()
        user.userName = userName
        user.password = password

        // Convert user into jsonstring
        val mapper = ObjectMapper()
        val requestBody = mapper.writeValueAsString(user)

        //
        val headers = getDefaultHeaders()
        val responseString = postRequest(URL_BASE + API_BASE + REQUEST_CREATE_USER, requestBody, headers)
        val response = mapper.readValue<UserResponse>(responseString)


        return response
    }

    /**
     *
     */
    fun loginUser(userName: String, password: String): UserResponse {

        val user = User(userName, password)

        val mapper = ObjectMapper()
        val requestBody = mapper.writeValueAsString(user)

        val header = addJSONHeaders(getDefaultHeaders())
        val responseString = postRequest(URL_BASE + API_BASE + REQUEST_LOGIN_USER, requestBody, header)
        val response = mapper.readValue<UserResponse>(responseString)

        return response
    }
}