package com.scottjay.mytests.androidtestproject.login

class LoginFormModel {

    var userName: String? = null
    var password: String? = null

    fun validate(): String? {
        if (userName == null) return "Username cannot be empty"
        if (password == null) return "Password cannot be empty"
        if (userName!!.isEmpty()) return "Username cannot be empty"
        if (password!!.isEmpty()) return "Password cannot be empty"

        return null
    }
}