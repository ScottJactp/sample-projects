package com.scottjay.mytests.androidtestproject

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.scottjay.mytests.androidtestproject.util.database.TextMessage

class MessagesViewModel: ViewModel() {


    fun getMessagesFromUser(userId: String): LiveData<List<TextMessage>> {
        return AppRepository.getTextMessages(userId)
    }

    fun insertMessage(message: TextMessage) {
        AppRepository.insertMessage(message)
    }

}