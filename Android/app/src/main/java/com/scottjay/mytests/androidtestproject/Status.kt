package com.scottjay.mytests.androidtestproject

class Status {
    companion object {
        enum class STATUS {
            SUCCESS,
            FAILED,
            WAITING
        }
    }

    var status: STATUS = STATUS.FAILED
    var reason: String = ""
}