package com.scottjay.mytests.androidtestproject.login

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import com.scottjay.mytests.androidtestproject.R
import com.scottjay.mytests.androidtestproject.Status

class LoginActivity: AppCompatActivity() {

    private val TAG = "LoginActivity"

    lateinit var viewModel: LoginViewModel

    lateinit var userNameET: EditText
    lateinit var passwordET: EditText
    lateinit var submitBtn: TextView
    lateinit var errorMessageTV: TextView
    lateinit var progressBar: ProgressBar

    lateinit var statusLiveData: LiveData<Status>

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        statusLiveData = viewModel.getStatusLiveData()

        userNameET = findViewById(R.id.activity_login_username_et)
        passwordET = findViewById(R.id.activity_login_password_et)
        submitBtn = findViewById(R.id.activity_login_submit_btn)
        errorMessageTV = findViewById(R.id.activity_login_error_tv)
        progressBar = findViewById(R.id.activity_login_pb)

        userNameET.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(s: Editable?) {
                viewModel.setUserName(s.toString())
            }
        })

        passwordET.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(s: Editable?) {
                viewModel.setPassword(s.toString())
            }
        })

        submitBtn.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            errorMessageTV.visibility = View.GONE
            viewModel.buttonClicked()
        }

        statusLiveData.observe(this, Observer {
            if (it != null) {
                if (it.status == Status.Companion.STATUS.SUCCESS) {
                    // Go to the next activity
                }
                else if (it.status == Status.Companion.STATUS.FAILED) {
                    errorMessageTV.text = it.reason
                    errorMessageTV.visibility = View.VISIBLE
                    progressBar.visibility = View.GONE
                }
            }
        })
    }
}