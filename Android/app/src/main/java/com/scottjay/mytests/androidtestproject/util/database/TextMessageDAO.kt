package com.scottjay.mytests.androidtestproject.util.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

@Dao
abstract class TextMessageDAO {

    @Query("SELECT * FROM messages")
    abstract fun selectAll(): List<TextMessage>

    @Query("SELECT * FROM messages WHERE to_id = :userId OR from_id = :userId ORDER BY message_time")
    abstract fun selectMessagesFromUser(userId: String): LiveData<List<TextMessage>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(textMessage: TextMessage)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(vararg textMessages: TextMessage)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(textMessages: List<TextMessage>)

    @Delete
    abstract fun delete(textMessage: TextMessage)
}