package com.scottjay.mytests.androidtestproject.util.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import android.util.Log
import org.json.JSONException
import org.json.JSONObject

/*
TextMessage json structure
    {"messageId": "jqoefbvqewfv",   Id of the message itself
    "from_id": "efvinqwvpj",        Id of person sending the message
    "to_id": "wbfjvnwefv",          Id of person receiving the message
    "messageTime": 234949582,       TextMessage sent time in milliseconds
    "messageBody": "hello"}
 */

@Entity(tableName = "messages",
        indices = arrayOf(Index(value = ["message_id"], unique = true)))
data class TextMessage(
        @PrimaryKey(autoGenerate = true) var id: Int = 0,
        @ColumnInfo(name = "message_id") var messageId: String? = null,
        @ColumnInfo(name = "from_id") var fromId: String? = null,
        @ColumnInfo(name = "to_id") var toId: String? = null,
        @ColumnInfo(name = "message_body") var messageBody: String? = null,
        @ColumnInfo(name = "message_time") var messageTime: Long = 0L) {

    companion object {
        const val TAG = "TextMessage"

        fun parseMessage(jsonObject: JSONObject): TextMessage? {
            Log.e(TAG, jsonObject.toString())

            val mId = jsonObject.optString("id", "")
            val mFromId = jsonObject.optString("fromId", "")
            val mToId = jsonObject.optString("toId", "")
            val mMessageTime = jsonObject.optLong("messageTime", 0L)
            val mMessageBody = jsonObject.optString("messageBody", "")
            if (mId.length <= 0) {
                return null
            }
            else
                return TextMessage(messageId = mId, fromId = mFromId, toId = mToId, messageTime = mMessageTime, messageBody = mMessageBody)
        }

        fun parseMessage(jsonString: String): TextMessage? {
            try {
                return parseMessage(JSONObject(jsonString))
            }
            catch (e: JSONException) {
                Log.e(TAG, "Failed: " + e.message)
                return null
            }
        }

        fun messageToString(textMessage: TextMessage): String {
            return messageToJSONObject(textMessage).toString()
        }

        fun messageToJSONObject(textMessage: TextMessage): JSONObject {
            return JSONObject()
                    .put("id", textMessage.messageId)
                    .put("fromId", textMessage.fromId)
                    .put("toId", textMessage.toId)
                    .put("messageTime", textMessage.messageTime)
                    .put("messageBody", textMessage.messageBody)
        }
    }
}