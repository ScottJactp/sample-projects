package com.scottjay.mytests.androidtestproject.login

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.scottjay.mytests.androidtestproject.AppRepository
import com.scottjay.mytests.androidtestproject.Status

class LoginViewModel: ViewModel() {

    val TAG = "LoginViewModel"

    // create and house a login model
    // have functions for interacting with that login model

    val statusLiveData = MutableLiveData<Status>() // Will update the ui on the status of the login

    val loginFormModel = LoginFormModel() // Model holding the state of the login form

    fun setUserName(userName: String) {
        loginFormModel.userName = userName
    }

    fun setPassword(password: String) {
        loginFormModel.password = password
    }

    fun buttonClicked()  {
        val validation = loginFormModel.validate()
        if (loginFormModel.validate() == null) {
            // Do stuff to process login
            AppRepository.loginUser(loginFormModel.userName!!, loginFormModel.password!!, statusLiveData)
        }
        else {
            val newStatus = Status()
            newStatus.status = Status.Companion.STATUS.FAILED
            newStatus.reason = validation!!
            statusLiveData.postValue(newStatus)
        }
    }

    fun getStatusLiveData(): LiveData<Status> {
        return statusLiveData
    }
}