package com.scottjay.mytests.androidtestproject

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.scottjay.mytests.androidtestproject.util.database.TextMessage

class MessagesFragment: Fragment() {

    var mRecyclerView: RecyclerView? = null
    var mRecyclerAdapter: TextMessagesRecyclerAdapter? = null

    var mAdapterList: List<TextMessage> = listOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_messages, container, false)

        mRecyclerView = view.findViewById<RecyclerView>(R.id.messages_recycler_view)
        mRecyclerView?.layoutManager = LinearLayoutManager(activity)
        mRecyclerAdapter = TextMessagesRecyclerAdapter()
        mRecyclerView?.adapter = mRecyclerAdapter
        mRecyclerAdapter?.changeList(mAdapterList)

        return view
    }

    fun updateMessages(list: List<TextMessage>?) {
        Log.e("MessagesFragment", "updating messages ${list}")
        list?.let {
            mAdapterList = it
            Log.e("MessagesFragment", "updatingrecycler")
            Log.e("MessagesFragment", "$mRecyclerAdapter")
            mRecyclerAdapter?.changeList(list)}
    }
}