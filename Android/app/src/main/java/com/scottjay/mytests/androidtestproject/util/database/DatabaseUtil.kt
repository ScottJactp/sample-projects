package com.scottjay.mytests.androidtestproject.util.database

import android.app.Application
import android.arch.persistence.room.*

object DatabaseUtil {
    private const val DATABASE_NAME = "sample_android_database"
    private const val DATABASE_VERSION = 2

    private lateinit var db: AppDatabase

    fun getDB(app: Application): AppDatabase {
        if (!::db.isInitialized) {
            db = Room.databaseBuilder(
                    app,
                    AppDatabase::class.java,
                    DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build()
        }
        return db
    }

    @Database(entities = arrayOf(TextMessage::class), version = DATABASE_VERSION)
    abstract class AppDatabase: RoomDatabase() {
        abstract fun textMessageDAO(): TextMessageDAO
    }
}