package com.scottjay.mytests.androidtestproject

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.scottjay.mytests.androidtestproject.login.LoginActivity
import com.scottjay.mytests.androidtestproject.util.database.TextMessage

class MainActivity : AppCompatActivity() {

    companion object {
        const val TAG = "MainActivity"
    }

    private var viewModel: MessagesViewModel? = null

    private var messagesLiveData: LiveData<List<TextMessage>>? = null

    private lateinit var messagesFragment: MessagesFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(MessagesViewModel::class.java)
        messagesLiveData = viewModel?.getMessagesFromUser("A")

        messagesFragment = MessagesFragment()
        messagesFragment.updateMessages(messagesLiveData?.value)

        messagesLiveData?.observe(this, object: Observer<List<TextMessage>> {
            override fun onChanged(t: List<TextMessage>?) {
                Log.e(TAG, "onchange: ${t?.size}")
                messagesFragment.updateMessages(t)
            }
        })

        val hello = findViewById<TextView>(R.id.hello_world)
        hello.setOnClickListener { view -> run {
            supportFragmentManager.beginTransaction().add(R.id.activity_main_fragment_fl, messagesFragment).commit()
        } }

        Thread{
            viewModel?.insertMessage(TextMessage(messageId = "i", fromId = "jj", toId = "mm", messageTime = 110, messageBody = "test"))
        }


        findViewById<TextView>(R.id.launch_login).setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

    }
}
