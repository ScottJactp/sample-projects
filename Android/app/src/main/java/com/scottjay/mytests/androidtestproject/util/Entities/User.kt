package com.scottjay.mytests.androidtestproject.util.Entities

class User {

    var id: String? = null
    var userName: String? = null
    var authToken: String? = null
    var password: String? = null

    constructor()

    constructor(userName: String, password: String) {
        this.userName = userName
        this.password = password
    }

}