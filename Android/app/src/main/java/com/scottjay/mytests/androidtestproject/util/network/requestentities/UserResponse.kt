package com.scottjay.mytests.androidtestproject.util.network.requestentities

import com.scottjay.mytests.androidtestproject.util.Entities.User

class UserResponse() {

    var code: String = ""
    var reason: String = ""
    var user: User? = null

    override fun toString(): String {
        return "UserResponse={'code': '$code', 'reason': '$reason', 'user': $user}"
    }
}